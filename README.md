# AWS SDK for JavaScript

A fork of the official AWS SDK for JavaScript that does not automatically load credentials and region from the run-time environment. Services do not inherit credentials or region from `AWS.config`. Therefore, all service instances must have their credentials and region set individually--providing better encapsulation of service clients in single threaded, multi-user environments.

Release notes for the original can be found at http://aws.amazon.com/releasenotes/SDK/JavaScript

## Installing

```sh
npm install aws-sdk-multi-user
```

## Usage and Getting Started

```javascript
var AWS = require("aws-sdk-multi-user"); // AWS.config is an immutable object with no credentials
var ec2 = new AWS.EC2( { region: "us-east-1" } ); // ec2 has no credentials
ec2.describeRegions(); // Fails with `Credentials not set` error
var credentials = new AWS.Credentials( id, secretKey ); // Creates a credential object
ec2.config.credentials = credentials; 
ec2.describeRegions(); // Succeeds if credentials are valid
var anotherEc2 = new AWS.EC2( { region: "us-west-2" } ); // has no credentials
anotherEc2.describeRegions(); // Fails with `Credentials not set` error
( anotherEc2.config.region === ec2.config.region ); // Returns false
( AWS.config.region === ec2.config.region ); // Returns false
( AWS.config.region === anotherEc2.config.region ); // Returns false
AWS.config.region; // null
AWS.config.region = "us-west-2";
AWS.config.region; // null
( AWS.config.region === anotherEc2.config.region ); // Returns false
```

## License

This SDK is distributed under the
[Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0),
see LICENSE.txt and NOTICE.txt for more information.